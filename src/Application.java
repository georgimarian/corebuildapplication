import java.util.ArrayList;
import java.util.List;

import controller.Controller;
import model.Character;
import model.JSONRead;
import model.Planet;
import view.View;

public class Application {
	
	public static void main(String[] args) {
		List<Character> characters = new ArrayList<Character>();
		List<Character> superHeroes = new ArrayList<Character>();
		List<Character> villains = new ArrayList<Character>();
		List<Planet> planets = new ArrayList<Planet>();

		try {
			characters = JSONRead.parseCharacters("characters.json");
		} catch (Exception e1) {
			System.out.println("Error while parsing characters.json");
		}
		for(Character c: characters) {
			if(c.isVillain()) {
				villains.add(c);
			}
			else {
				superHeroes.add(c);
			}
		}
	
		try {
			planets = JSONRead.parsePlanet("planets.json");
		} catch (Exception e1) {
			System.out.println("Error while parsing planets.json");

		}
		View view = new View(planets,superHeroes,villains);
		Controller controller = new Controller(view);
		controller.start();
	}
}
