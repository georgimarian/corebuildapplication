package model;

public class Character {
	private int id;
	private String name;
	private String description;
	private int health;
	private int attack;
	boolean isVillain;
	
	public Character(long id2, String name2, String description2, long health, long attack2, boolean isVillain2) {
		this.id = (int) id2;
		this.name = name2;
		this.description = description2;
		this.health = (int) health;
		this.attack = (int) attack2;
		this.isVillain = isVillain2;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public int getAttack() {
		return attack;
	}

	public void setAttack(int attack) {
		this.attack = attack;
	}

	public boolean isVillain() {
		return isVillain;
	}

	public void setVillain(boolean isVillain) {
		this.isVillain = isVillain;
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("id: " + this.id);
		builder.append(" name: " + name);
		builder.append(" description: " + description);
		builder.append(" attack: " + attack);
		builder.append(" health: " + health);
		builder.append(" isVillain: " + isVillain);
		return builder.toString();
	}
}
