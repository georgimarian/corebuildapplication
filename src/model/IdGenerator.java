package model;

public class IdGenerator {
	 
	static int counter = 0;
	private IdGenerator()	{	
	}
	
	public static int getNextId()
	{
		return counter++;
	}
 
}

