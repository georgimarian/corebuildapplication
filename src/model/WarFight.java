package model;

import java.util.Random;

import view.View;

public class WarFight {
	private Planet planet;
	private Character superHero;
	private Character villain;
	private View view;
	
	public WarFight(Planet planet, Character hero, Character villain) {
		this.setPlanet(planet);
		this.setHero(hero);
		this.setVillain(villain);
	}

	public void applyPlanetModifiers() {
		view.setTextAreaText("Some planet modifiers willbe applied");
		superHero.setAttack(superHero.getAttack()+ planet.getHeroAttackModifier());
		superHero.setHealth(superHero.getHealth()+planet.getHeroHealthModifier());
		villain.setAttack(villain.getAttack()+planet.getVillainAttackModifier());
		villain.setHealth(villain.getHealth()+planet.getVillainHealthModifier());
		view.setTextAreaText("\nModifiers have been applied!");
	}
	

	public void letTheWarBegin() {
		Random random = new Random();
		boolean finished = false;
		while(!finished) {
			int attackHero = superHero.getAttack()*(random.nextInt(100 - 60 + 1) + 60)/100;
			view.setTextAreaText("\n Hero attack " + attackHero);
			villain.setHealth(villain.getHealth()-attackHero);
			logProgress();
			view.getContentPane().revalidate();
			view.getContentPane().repaint();
			if(villain.getHealth() <= 0) {
				finished = true;
				break;
			}
			else {
				int attackVillain = villain.getAttack()*(random.nextInt(100 - 60 + 1) + 60)/100;
				view.setTextAreaText("\n Villain attack: " + attackVillain);
				superHero.setHealth(superHero.getHealth()-attackVillain);
				if(superHero.getHealth() <= 0) {
					finished = true;
					break;
				}
				logProgress();
				view.getContentPane().revalidate();
				view.getContentPane().repaint();
			}
		}
		logProgress();
		view.getContentPane().revalidate();
		view.getContentPane().repaint();
		if( superHero.getHealth() < 0)	{
			view.setTextAreaText("\n\nVillain won");
			view.getContentPane().revalidate();
			view.getContentPane().repaint();
		}
		else {
			view.setTextAreaText("\n\nHero won");
			view.getContentPane().revalidate();
			view.getContentPane().repaint();
		}
		

	}
	
	public void logProgress() {
		view.setTextAreaText(superHero.getName() + " " + superHero.getHealth() + "\n" + villain.getName() + " " + villain.getHealth());
	}
	
	public Character getHero() {
		return superHero;
	}

	public void setHero(Character hero) {
		this.superHero = hero;
	}

	public Character getVillain() {
		return villain;
	}

	public void setVillain(Character villain) {
		this.villain = villain;
	}

	public Planet getPlanet() {
		return planet;
	}

	public void setPlanet(Planet planet) {
		this.planet = planet;
	}

	public View getView() {
		return view;
	}

	public void setView(View view) {
		this.view = view;
	}
}
