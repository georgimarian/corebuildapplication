package model;

public class Planet {
	private int id;
	private String name;
	private String description;
	private int heroAttackModifier;
	private int heroHealthModifier;
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getHeroAttackModifier() {
		return heroAttackModifier;
	}

	public void setHeroAttackModifier(int heroAttackModifier) {
		this.heroAttackModifier = heroAttackModifier;
	}

	public int getHeroHealthModifier() {
		return heroHealthModifier;
	}

	public void setHeroHealthModifier(int heroHealthModifier) {
		this.heroHealthModifier = heroHealthModifier;
	}

	public int getVillainAttackModifier() {
		return villainAttackModifier;
	}

	public void setVillainAttackModifier(int villainAttackModifier) {
		this.villainAttackModifier = villainAttackModifier;
	}

	public int getVillainHealthModifier() {
		return villainHealthModifier;
	}

	public void setVillainHealthModifier(int villainHealthModifier) {
		this.villainHealthModifier = villainHealthModifier;
	}

	private int villainAttackModifier;
	private int villainHealthModifier;
	
	public Planet(long id2, String name2, String description2, long heroAttackModifier2, long heroHealthModifier2, long villainAttackModifier2, long villainHealthModifier2) {
		this.id = (int) id2;
		this.name = name2;
		this.description = description2;
		this.heroAttackModifier = (int) heroAttackModifier2;
		this.heroHealthModifier = (int) heroHealthModifier2;
		this.villainAttackModifier = (int) villainAttackModifier2;
		this.villainHealthModifier = (int) villainHealthModifier2;
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("id: " + this.id);
		builder.append(" name: " + name);
		builder.append(" description: " + description);
		builder.append(" h attack: " + heroAttackModifier);
		builder.append(" h health: " + heroHealthModifier);
		builder.append(" v attack: " + villainAttackModifier);
		builder.append(" v health: " + villainHealthModifier);
		return builder.toString();
	}
	
}
