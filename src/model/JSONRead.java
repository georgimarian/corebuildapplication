package model;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.*;

public class JSONRead {
	public static List<Character> parseCharacters(String filename) throws Exception  
    { 
		List<Character> result = new ArrayList<Character>();
        // parsing file 
        Object obj = new JSONParser().parse(new FileReader(filename)); 
        // typecasting obj to JSONArray
        JSONArray jo = (JSONArray) obj; 
        //Iterate array to find JSONObjects and transform them into Model objects
        for(int i=0;i<jo.size();i++){
            JSONObject o = (JSONObject) jo.get(i);
            long id = (long) o.get("id");
            String name = (String) o.get("name"); 
            String description = (String) o.get("description"); 
            
            long health = (long) o.get("health"); 
            long attack = (long) o.get("attack");
            boolean isVillain = (boolean) o.get("isVillain");           
            Character c = new Character(id,name,description,health,attack,isVillain);
            result.add(c);
        }     
        return result;
    }
	
	public static List<Planet> parsePlanet(String filename) throws Exception  
    { 
		List<Planet> result = new ArrayList<Planet>();
		long heroAttackModifier = 0;
		long heroHealthModifier = 0;
		long villainHealthModifier = 0;
		long villainAttackModifier = 0;
        // parsing file 
        Object obj = new JSONParser().parse(new FileReader(filename)); 
        // type cast obj to JSONArray
        JSONArray jo = (JSONArray) obj; 
        //Iterate array to find JSONObjects and transform them into Model objects
        for(int i=0;i<jo.size();i++){
            JSONObject o = (JSONObject) jo.get(i);
            long id = (long) o.get("id");
            String name = (String) o.get("name"); 
            String description = (String) o.get("description");
            JSONObject modifiers = (JSONObject) o.get("modifiers"); 
            for(int j=0;j<modifiers.size();j++) {
            heroAttackModifier = (long) modifiers.get("heroAttackModifier"); 
            heroHealthModifier = (long) modifiers.get("heroHealthModifier");
            villainHealthModifier = (long) modifiers.get("villainHealthModifier");
            villainAttackModifier = (long) modifiers.get("villainAttackModifier");
            }
            Planet p = new Planet(id,name,description,heroAttackModifier,heroHealthModifier,villainAttackModifier,villainHealthModifier);
            result.add(p);
        }     
        return result;
    }

}
