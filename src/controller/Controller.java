package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import model.JSONRead;
import model.Planet;
import view.View;
import model.Character;
import model.WarFight;

public class Controller {
	private View view;

	public Controller(View view) {
		this.view = view;
	}

	public void start() {
		try {

			view.setLocationRelativeTo(null);
			view.setVisible(true);

			initializeButtonListeners();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void initializeButtonListeners() {
		Planet planet = view.getSelectedPlanet();
		Character superHero = view.getSelectedSuperHero();
		Character villain = view.getSelectedVillain();
		view.addStartFightButtonActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				WarFight fight = new WarFight(planet, superHero, villain);
				fight.setView(view);
				fight.applyPlanetModifiers();
				fight.letTheWarBegin();
			}
		});
	}
}
