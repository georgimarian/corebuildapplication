package view;

import javax.swing.JFrame;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import model.Planet;
import model.Character;
import javax.swing.JButton;

public class View extends JFrame {
	JComboBox planetComboBox;
	JComboBox superHeroComboBox;
	JComboBox villainComboBox;
	JButton btnStartFight;
	JTextArea textArea;
	
	public View(List<Planet> planets, List<Character> superHeroes, List<Character> villains) {
		setFont(new Font("Bitstream Charter", Font.PLAIN, 14));
		setTitle("Marvel SuperHero Wars");
		getContentPane().setLayout(null);
		this.setBounds(100, 100, 750, 600);
		
		planetComboBox = new JComboBox();
		planetComboBox.setBounds(31, 22, 582, 30);
		for (Planet p : planets) {
			planetComboBox.addItem(p);
		}
		getContentPane().add(planetComboBox);
		
		superHeroComboBox = new JComboBox<Character>();
		superHeroComboBox.setBounds(31, 84, 582, 30);
		for (Character c : superHeroes) {
			superHeroComboBox.addItem(c);
		}
		getContentPane().add(superHeroComboBox);
		
		villainComboBox = new JComboBox();
		villainComboBox.setBounds(31, 143, 582, 36);
		for (Character c : villains) {
			villainComboBox.addItem(c);
		}
		getContentPane().add(villainComboBox);
		
		JLabel lblSelectSuperhero = new JLabel("Select Planet:");
		lblSelectSuperhero.setBounds(31, 0, 165, 15);
		getContentPane().add(lblSelectSuperhero);
		
		JLabel lblSelectVillain = new JLabel("Select hero:");
		lblSelectVillain.setBounds(30, 57, 122, 15);
		getContentPane().add(lblSelectVillain);
		
		JLabel lblSelectPlanet = new JLabel("Select villain:");
		lblSelectPlanet.setBounds(31, 121, 153, 10);
		getContentPane().add(lblSelectPlanet);
		
		
		JScrollPane scroll = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scroll.setSize(389, 302);
		scroll.setLocation(300, 230);
		getContentPane().add(scroll);
		
		textArea = new JTextArea();
		scroll.setViewportView(textArea);
		btnStartFight = new JButton("Start Fight");
		btnStartFight.setBounds(49, 213, 114, 25);
		getContentPane().add(btnStartFight);
	}
	
	public void addStartFightButtonActionListener(final ActionListener actionListener) {
		btnStartFight.addActionListener(actionListener);
	}
	

	public Character getSelectedSuperHero() {
		return (Character) superHeroComboBox.getSelectedItem();
	}
	public Character getSelectedVillain() {
		return (Character) villainComboBox.getSelectedItem();
	}
	public Planet getSelectedPlanet() {
		return (Planet) planetComboBox.getSelectedItem();
	}
	
	public String getTextAreaText() {
		return textArea.getText();
	}
	
	public void setTextAreaText(String string) {
		textArea.setText(textArea.getText() + " " + string);
	}


}
